#!/bin/bash
set -o allexport; source .env; set +o allexport
BOT_NAME="${BOT_NAME}" BOT_TOKEN="${BOT_TOKEN}" HTTP_PORT=8000 LLM=tinyllama OLLAMA_BASE_URL=http://host.docker.internal:11434 docker compose --profile hook up
