# hook-demo

## JSON to Go struct

- https://transform.tools/json-to-go

## GitLab Webhooks

- https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
- https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html


## How to run this project

### Classic way

```bash
go run main.go
# or
OLLAMA_BASE_URL="http://localhost:11434" LLM="tinyllama" go run main.go
```
> you can omit the `OLLAMA_BASE_URL` and `LLM` if you want to use the default ones

Test the hook:
```bash
HOOK_DOMAIN="http://localhost:8080" ./note-event.sh
HOOK_DOMAIN="http://localhost:8080" ./note-yodamad.sh
HOOK_DOMAIN="http://localhost:8080" ./note-keegorg.sh
```
> you can omit the `HOOK_DOMAIN` if you want to use the default one listening on 8080

#### Remark about the prompt

The content of the prompt is defined in the JSON payload of the GitLab webhook comment event, with the field `object_attributes.note`:
```json
  "object_attributes": {
    "id": 1241,
    "note": "Who is Jean-Luc Picard?",
  }
```
> Look at the `note-event.sh` script.

### With Docker Compose

#### Ollama is installed directly on the host (the workstation)

- Ollama is running locally
- You hook is running in a container

Start the stack:
```bash
HTTP_PORT=9999 LLM=tinyllama OLLAMA_BASE_URL=http://host.docker.internal:11434 docker compose --profile hook up
```

Test the hook:
```bash
HOOK_DOMAIN="http://localhost:9999" ./note-event.sh
HOOK_DOMAIN="http://localhost:9999" ./note-yodamad.sh
HOOK_DOMAIN="http://localhost:9999" ./note-keegorg.sh
```

#### Run Ollama in a container

Start the stack:
```bash
HTTP_PORT=8888 LLM=qwen:0.5b OLLAMA_BASE_URL=http://ollama:11434 docker compose --profile container up
```

Test the hook:
```bash
HOOK_DOMAIN="http://localhost:8888" ./note-event.sh
HOOK_DOMAIN="http://localhost:8888" ./note-yodamad.sh
HOOK_DOMAIN="http://localhost:8888" ./note-keegorg.sh
```

#### Use the watch mode

With the `watch` mode, every time you will change something in your Go code, the build of the app will be triggered.

```bash
HTTP_PORT=9999 LLM=tinyllama OLLAMA_BASE_URL=http://host.docker.internal:11434 docker compose --profile watch
```

```bash
HTTP_PORT=8888 LLM=qwen:0.5b OLLAMA_BASE_URL=http://ollama:11434 docker compose --profile container watch
```

#### Remark

👋 Do not forget to stop the compose stack when you don't need it (to avoid HTTP port conflicts).
