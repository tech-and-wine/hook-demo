package gitlab

type WebHookEvent struct {
	ObjectKind string `json:"object_kind"`
	EventType  string `json:"event_type"`
}
