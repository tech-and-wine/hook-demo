FROM golang:1.22.1-alpine as buildernext
#FROM golang:latest as buildernext
WORKDIR /app
COPY . .
RUN go build

FROM scratch
WORKDIR /app
COPY --from=buildernext /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=buildernext /app/hook-demo .

CMD ["./hook-demo"]
