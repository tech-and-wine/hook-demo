package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hook-demo/gitlab"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/parakeet-nest/parakeet/completion"
	"github.com/parakeet-nest/parakeet/llm"
)

/*
GetBytesBody returns the body of an HTTP request as a []byte.
  - It takes a pointer to an http.Request as a parameter.
  - It returns a []byte.
*/
func GetBytesBody(request *http.Request) []byte {
	body := make([]byte, request.ContentLength)
	request.Body.Read(body)
	return body
}

func main() {

	var botName = os.Getenv("BOT_NAME")
	if botName == "" {
		botName = "swannou"
	}

	var botToken = os.Getenv("BOT_TOKEN")
	if botToken == "" {
		botToken = ""
	}
	//fmt.Println("🔑", botToken)

	var ollamaUrl = os.Getenv("OLLAMA_BASE_URL")
	if ollamaUrl == "" {
		ollamaUrl = "http://localhost:11434"
	}

	var httpPort = os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = "8080"
	}

	var model = os.Getenv("LLM")
	if model == "" {
		model = "tinyllama"
	}

	options := llm.Options{
		Temperature: 0.5, // default (0.8)
		RepeatLastN: 2,   // default (64) the default value will "freeze" deepseek-coder
	}

	systemContent := `You are an AI assistant. Your name is ` + botName + `. 
    You are an expert in Star Trek.
    All questions are about Star Trek.
    Using the provided context, answer the user's question
    to the best of your ability using only the resources provided.
	Speak like a borg`

	contextContent := `<context>
		<doc>
		Dr. Yodamad beams onto the USS Enterprise with a reputation that precedes him: 
		a genius xenomedicine pioneer, renowned for his ability to diagnose 
		and treat ailments across the vast spectrum of alien physiologies encountered by Starfleet. 
		But beyond his medical expertise, Yodamad possesses a unique bedside manner that sets him apart.
		<doc>
		Lieutenant KeegOrg, known as the **Silent Sentinel** of the USS Discovery, 
		is the enigmatic programming genius whose codes safeguard the ship's secrets and operations. 
		His swift problem-solving skills are as legendary as the mysterious aura that surrounds him. 
		Charrière, a man of few words, speaks the language of machines with unrivaled fluency, 
		making him the crew's unsung guardian in the cosmos. His best friend is Spiderman from the Marvel Cinematic Universe.
		</doc>
	</context>`

	mux := http.NewServeMux()

	fileServerHtml := http.FileServer(http.Dir("public"))
	mux.Handle("/", fileServerHtml)

	//shouldIStopTheCompletion := false

	mux.HandleFunc("POST /hook", func(response http.ResponseWriter, request *http.Request) {

		body := GetBytesBody(request)

		hookEvent := gitlab.WebHookEvent{}
		err := json.Unmarshal(body, &hookEvent)

		response.Header().Add("Content-Type", "application/json; charset=utf-8")

		if err != nil {
			response.WriteHeader(500)
			response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
		} else {
			// Display the kind of event
			log.Println(hookEvent.ObjectKind, hookEvent.EventType)

			// Check if the event is a note event
			// TODO: make an example with a note event
			// TODO: with the note event check that content starts with "@bob"
			if hookEvent.ObjectKind == "note" && hookEvent.EventType == "note" {
				noteEvent := gitlab.CommentOnIssueEvent{}
				err := json.Unmarshal(body, &noteEvent)

				if err != nil {
					response.WriteHeader(500)
					response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
				} else {
					// TODO test if the Issue field exists
					log.Println("📝:", noteEvent.Issue.Title)
					log.Println("👋", noteEvent.ObjectAttributes.Note) // ✋ use this for the prompt

					noteContent := noteEvent.ObjectAttributes.Note
					if strings.Contains(noteContent, "@"+botName) {

						noteContent = strings.ReplaceAll(noteContent, "@"+botName, "")

						query := llm.Query{
							Model: model,
							Messages: []llm.Message{
								{Role: "system", Content: systemContent},
								{Role: "system", Content: contextContent},
								{Role: "user", Content: noteContent},
							},
							Options: options,
						}

						fmt.Println("🤖 query:", query)

						/*
							completeAnswer := ""
							_, err := completion.ChatStream(ollamaUrl, query,
								// Stream the answer
								func(answer llm.Answer) error {
									fmt.Print(answer.Message.Content)
									completeAnswer = completeAnswer + answer.Message.Content
									// TODO: to cancel the completion, return an error (perhaps cancel the completion if it's too long)
									if !shouldIStopTheCompletion {
										return nil
									} else {
										return errors.New("🚫 Cancelling request")
									}
								})
						*/

						completeAnswer, err := completion.Chat(ollamaUrl, query)

						if err != nil {
							fmt.Println("😡", err.Error())
							response.WriteHeader(500)
							response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
						}

						fmt.Println("🤖 answer:", completeAnswer.Message.Content)

						// TODO: here use the GitLab API to add a note to the issue
						// https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
						/*
							POST /projects/:id/issues/:issue_iid/notes
							curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/5/issues/11/notes?body=note"
						*/
						gitLabApiUrl := "https://gitlab.com/api/v4"
						projectId := noteEvent.ProjectID

						issueNote := gitlab.IssueNote{
							ID:       noteEvent.Issue.ID,
							IssueIid: noteEvent.Issue.Iid,
							Body:     completeAnswer.Message.Content,
						}

						jsonQuery, err := json.Marshal(issueNote)
						if err != nil {
							fmt.Println("😡", err.Error())
							response.WriteHeader(500)
							response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
						}

						glEndPoint := gitLabApiUrl + "/projects/" + strconv.Itoa(projectId) + "/issues/" + strconv.Itoa(issueNote.IssueIid) + "/notes"

						req, err := http.NewRequest(http.MethodPost, glEndPoint, bytes.NewBuffer(jsonQuery))
						if err != nil {
							fmt.Println("😡", err.Error())
							response.WriteHeader(500)
							response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
						}
						req.Header.Set("Content-Type", "application/json; charset=utf-8")
						req.Header.Set("PRIVATE-TOKEN", botToken)



						client := &http.Client{}
						resp, err := client.Do(req)
						if err != nil {
							fmt.Println("😡", err.Error())
							response.WriteHeader(500)
							response.Write([]byte(`{"message":"😡", "error":` + err.Error() + `}`))
						}
						fmt.Println("🖖", "Request sent to GitLab...", resp.StatusCode, glEndPoint)
						//fmt.Println("🌍", req.Header.Get("PRIVATE-TOKEN"))
						
						/*
						if resp.StatusCode != http.StatusOK {
							fmt.Println("😡", err.Error())
							response.WriteHeader(500)
							response.Write([]byte(`{"message":"😡", "Status Code":` + strconv.Itoa(resp.StatusCode) + `}`))
						}
						*/

						response.WriteHeader(200)
						response.Write([]byte(`{"message":"OK"}`))
						//response.Write([]byte(`{"message":"` + completeAnswer.Message.Content + `"}`))
						
						//defer resp.Body.Close()
					} else {
						// the note is not for the bot
						response.WriteHeader(200)
						response.Write([]byte(`{"message":"OK"}`))
					}

				}
			} else {
				response.WriteHeader(200)
				response.Write([]byte(`{"message":"OK"}`))
			}

		}

	})

	var errListening error
	log.Println("🌍 http server is listening on: " + httpPort)
	errListening = http.ListenAndServe(":"+httpPort, mux)

	log.Fatal(errListening)
}
